import Link from 'next/link'

export default function NavBar() {
  return (
    <nav>
        <h1> <Link href="/">My Blogs </Link> </h1>
        <p>Better thinking ...</p>
    </nav>
  )
}
