import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { getAllArticles } from '../utils'
import dayjs from 'dayjs'
import  NavBar from '../data/components/navbar'

export async function getStaticProps() {
  const articles = await getAllArticles()

  articles
    .map((article) => article.data)
    .sort((a, b) => {
      if (a.data.publishedAt > b.data.publishedAt) return 1
      if (a.data.publishedAt < b.data.publishedAt) return -1

      return 0
    })

  return {
    props: {
      posts: articles.reverse(),
    },
  }
}

export default function Home({ posts }) {
  return (
   <React.Fragment>
      <Head>
        <title>My Tutorial Blog Post</title>
      </Head>

      <main>
        <NavBar />
        {posts.map((frontMatter, i) => {
          return (
            <Link key={i} href={`/blog/${frontMatter.slug}`} passHref>
              <div>
                <h1 className="title">{frontMatter.title}</h1>
                <p className="summary">{frontMatter.excerpt}</p>
                <p className="date">
                  {dayjs(frontMatter.publishedAt).format('MMMM D, YYYY')} &mdash;{' '}
                  {frontMatter.readingTime}
                </p>
              </div>
            </Link>
          )
        })}
      </main>

    </React.Fragment>
  )
}
